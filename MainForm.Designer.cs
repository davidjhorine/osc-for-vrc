﻿
namespace VRC_OSC_Testing
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TPe131 = new System.Windows.Forms.TabPage();
            this.BtnE131StartStop = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RBe131Hsl = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.RBe131Rgb = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NumFloatMax = new System.Windows.Forms.NumericUpDown();
            this.NumE131SendPort = new System.Windows.Forms.NumericUpDown();
            this.NumFloatMin = new System.Windows.Forms.NumericUpDown();
            this.TxtRAddr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtGAddr = new System.Windows.Forms.TextBox();
            this.TxtBAddr = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.PnlColorPreview = new System.Windows.Forms.Panel();
            this.NumUniverse = new System.Windows.Forms.NumericUpDown();
            this.BtnColorPreview = new System.Windows.Forms.Button();
            this.TCMain = new System.Windows.Forms.TabControl();
            this.TPe131.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumFloatMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumE131SendPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumFloatMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUniverse)).BeginInit();
            this.TCMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // TPe131
            // 
            this.TPe131.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.TPe131.Controls.Add(this.BtnE131StartStop);
            this.TPe131.Controls.Add(this.groupBox1);
            this.TPe131.Controls.Add(this.label4);
            this.TPe131.Controls.Add(this.PnlColorPreview);
            this.TPe131.Controls.Add(this.NumUniverse);
            this.TPe131.Controls.Add(this.BtnColorPreview);
            this.TPe131.Location = new System.Drawing.Point(4, 22);
            this.TPe131.Name = "TPe131";
            this.TPe131.Padding = new System.Windows.Forms.Padding(3);
            this.TPe131.Size = new System.Drawing.Size(526, 335);
            this.TPe131.TabIndex = 0;
            this.TPe131.Text = "E1.31 to OSC";
            // 
            // BtnE131StartStop
            // 
            this.BtnE131StartStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(142)))), ((int)(((byte)(120)))));
            this.BtnE131StartStop.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(142)))), ((int)(((byte)(120)))));
            this.BtnE131StartStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(212)))), ((int)(((byte)(170)))));
            this.BtnE131StartStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnE131StartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnE131StartStop.ForeColor = System.Drawing.Color.White;
            this.BtnE131StartStop.Location = new System.Drawing.Point(409, 285);
            this.BtnE131StartStop.Name = "BtnE131StartStop";
            this.BtnE131StartStop.Size = new System.Drawing.Size(111, 42);
            this.BtnE131StartStop.TabIndex = 15;
            this.BtnE131StartStop.Text = "Start";
            this.BtnE131StartStop.UseVisualStyleBackColor = false;
            this.BtnE131StartStop.Click += new System.EventHandler(this.BtnE131StartStop_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RBe131Hsl);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.RBe131Rgb);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.NumFloatMax);
            this.groupBox1.Controls.Add(this.NumE131SendPort);
            this.groupBox1.Controls.Add(this.NumFloatMin);
            this.groupBox1.Controls.Add(this.TxtRAddr);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtGAddr);
            this.groupBox1.Controls.Add(this.TxtBAddr);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 260);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OSC Settings";
            // 
            // RBe131Hsl
            // 
            this.RBe131Hsl.AutoSize = true;
            this.RBe131Hsl.Location = new System.Drawing.Point(63, 236);
            this.RBe131Hsl.Name = "RBe131Hsl";
            this.RBe131Hsl.Size = new System.Drawing.Size(46, 17);
            this.RBe131Hsl.TabIndex = 16;
            this.RBe131Hsl.Text = "HSL";
            this.RBe131Hsl.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Output as";
            // 
            // RBe131Rgb
            // 
            this.RBe131Rgb.AutoSize = true;
            this.RBe131Rgb.Checked = true;
            this.RBe131Rgb.Location = new System.Drawing.Point(9, 236);
            this.RBe131Rgb.Name = "RBe131Rgb";
            this.RBe131Rgb.Size = new System.Drawing.Size(48, 17);
            this.RBe131Rgb.TabIndex = 14;
            this.RBe131Rgb.TabStop = true;
            this.RBe131Rgb.Text = "RGB";
            this.RBe131Rgb.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Send Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Output float min/max";
            // 
            // NumFloatMax
            // 
            this.NumFloatMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NumFloatMax.DecimalPlaces = 5;
            this.NumFloatMax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NumFloatMax.Location = new System.Drawing.Point(90, 194);
            this.NumFloatMax.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.NumFloatMax.Name = "NumFloatMax";
            this.NumFloatMax.Size = new System.Drawing.Size(83, 20);
            this.NumFloatMax.TabIndex = 12;
            this.NumFloatMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // NumE131SendPort
            // 
            this.NumE131SendPort.Location = new System.Drawing.Point(66, 152);
            this.NumE131SendPort.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.NumE131SendPort.Maximum = new decimal(new int[] {
            25565,
            0,
            0,
            0});
            this.NumE131SendPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumE131SendPort.Name = "NumE131SendPort";
            this.NumE131SendPort.Size = new System.Drawing.Size(72, 20);
            this.NumE131SendPort.TabIndex = 6;
            this.NumE131SendPort.Value = new decimal(new int[] {
            9000,
            0,
            0,
            0});
            // 
            // NumFloatMin
            // 
            this.NumFloatMin.DecimalPlaces = 5;
            this.NumFloatMin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NumFloatMin.Location = new System.Drawing.Point(9, 194);
            this.NumFloatMin.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.NumFloatMin.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.NumFloatMin.Name = "NumFloatMin";
            this.NumFloatMin.Size = new System.Drawing.Size(75, 20);
            this.NumFloatMin.TabIndex = 11;
            this.NumFloatMin.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // TxtRAddr
            // 
            this.TxtRAddr.Location = new System.Drawing.Point(9, 39);
            this.TxtRAddr.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.TxtRAddr.Name = "TxtRAddr";
            this.TxtRAddr.Size = new System.Drawing.Size(268, 20);
            this.TxtRAddr.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Red (RGB)/Hue (HSL) Address";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Green (RGB)/Saturation (HSL) Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Blue (RGB)/Lightness (HSL) Address";
            // 
            // TxtGAddr
            // 
            this.TxtGAddr.Location = new System.Drawing.Point(9, 81);
            this.TxtGAddr.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.TxtGAddr.Name = "TxtGAddr";
            this.TxtGAddr.Size = new System.Drawing.Size(268, 20);
            this.TxtGAddr.TabIndex = 3;
            // 
            // TxtBAddr
            // 
            this.TxtBAddr.Location = new System.Drawing.Point(9, 123);
            this.TxtBAddr.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            this.TxtBAddr.Name = "TxtBAddr";
            this.TxtBAddr.Size = new System.Drawing.Size(268, 20);
            this.TxtBAddr.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "E1.31 Listen Universe (Multicast)";
            // 
            // PnlColorPreview
            // 
            this.PnlColorPreview.Location = new System.Drawing.Point(297, 32);
            this.PnlColorPreview.Name = "PnlColorPreview";
            this.PnlColorPreview.Size = new System.Drawing.Size(221, 33);
            this.PnlColorPreview.TabIndex = 9;
            // 
            // NumUniverse
            // 
            this.NumUniverse.Location = new System.Drawing.Point(8, 285);
            this.NumUniverse.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.NumUniverse.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumUniverse.Name = "NumUniverse";
            this.NumUniverse.Size = new System.Drawing.Size(121, 20);
            this.NumUniverse.TabIndex = 8;
            this.NumUniverse.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BtnColorPreview
            // 
            this.BtnColorPreview.Enabled = false;
            this.BtnColorPreview.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(142)))), ((int)(((byte)(120)))));
            this.BtnColorPreview.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(142)))), ((int)(((byte)(120)))));
            this.BtnColorPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnColorPreview.Location = new System.Drawing.Point(297, 6);
            this.BtnColorPreview.Name = "BtnColorPreview";
            this.BtnColorPreview.Size = new System.Drawing.Size(221, 23);
            this.BtnColorPreview.TabIndex = 7;
            this.BtnColorPreview.Text = "Preview color for 5 seconds";
            this.BtnColorPreview.UseVisualStyleBackColor = true;
            this.BtnColorPreview.Click += new System.EventHandler(this.BtnColorPreview_Click);
            // 
            // TCMain
            // 
            this.TCMain.Controls.Add(this.TPe131);
            this.TCMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TCMain.Location = new System.Drawing.Point(0, 0);
            this.TCMain.Name = "TCMain";
            this.TCMain.SelectedIndex = 0;
            this.TCMain.Size = new System.Drawing.Size(534, 361);
            this.TCMain.TabIndex = 0;
            this.TCMain.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.TCMain_Selecting);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(534, 361);
            this.Controls.Add(this.TCMain);
            this.MaximumSize = new System.Drawing.Size(550, 400);
            this.MinimumSize = new System.Drawing.Size(550, 400);
            this.Name = "MainForm";
            this.Text = "Open Sound Control Experiments";
            this.TPe131.ResumeLayout(false);
            this.TPe131.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumFloatMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumE131SendPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumFloatMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumUniverse)).EndInit();
            this.TCMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage TPe131;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NumFloatMax;
        private System.Windows.Forms.NumericUpDown NumFloatMin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel PnlColorPreview;
        private System.Windows.Forms.NumericUpDown NumUniverse;
        private System.Windows.Forms.Button BtnColorPreview;
        private System.Windows.Forms.TextBox TxtBAddr;
        private System.Windows.Forms.TextBox TxtGAddr;
        private System.Windows.Forms.TextBox TxtRAddr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl TCMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown NumE131SendPort;
        private System.Windows.Forms.Button BtnE131StartStop;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton RBe131Rgb;
        private System.Windows.Forms.RadioButton RBe131Hsl;
    }
}

