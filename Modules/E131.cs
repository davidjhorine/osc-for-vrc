﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Kadmium_sACN.SacnReceiver;
using Rug.Osc;
using System.Net;

namespace VRC_OSC_Testing
{
    public partial class nodesigner { }
    public partial class MainForm : Form
    {

        MulticastSacnReceiverIPV4 E131Reciever;
        OscSender Sender;

        // Address/enabled values
        private string RAddr, GAddr, BAddr;
        private bool REnabled, GEnabled, BEnabled;

        // Float min/max values
        private float E131MultiplicationRange, E131Min;

        bool Running = false;
        private void BtnE131StartStop_Click(object sender, EventArgs e)
        {
            if (!Running)
            {
                // UI Updates
                SetDangerousControls(enabled: false);
                BtnColorPreview.Enabled = true;
                BtnE131StartStop.Text = "Stop";
                Running = true;

                // Create the OSC sender
                Sender = new OscSender(IPAddress.Parse("127.0.0.1"), localPort: 0, remotePort: 9000);
                Sender.Connect();

                // Store the current values of the address settings to avoid accessing
                // the UI thread on every recieve
                RAddr = TxtRAddr.Text;
                BAddr = TxtBAddr.Text;
                GAddr = TxtGAddr.Text;

                // Store if the packets should be sent to avoid checking if the strings
                // are empty every time
                REnabled = !string.IsNullOrEmpty(RAddr);
                BEnabled = !string.IsNullOrEmpty(BAddr);
                GEnabled = !string.IsNullOrEmpty(GAddr);

                // Get the range between the float min/max spinners for multiplication
                E131MultiplicationRange = (float)(NumFloatMax.Value - NumFloatMin.Value);
                E131Min = (float)NumFloatMin.Value;

                // Create the E131 listener
                E131Reciever = new MulticastSacnReceiverIPV4();

                // Assign events based on output type
                if (RBe131Hsl.Checked)
                    E131Reciever.OnDataPacketReceived += E131toHSL_OSC;
                else E131Reciever.OnDataPacketReceived += E131toRGB_OSC;

                E131Reciever.Listen(IPAddress.Any);
                E131Reciever.JoinMulticastGroup((ushort)NumUniverse.Value);
            }
            else
            {
                BtnColorPreview.Enabled = false;
                BtnE131StartStop.Text = "Start";
                Running = false;

                // Destroy the E131 listener
                E131Reciever.Dispose();
                E131Reciever = null;

                // Destroy the OSC Sender
                Sender.Dispose();
                Sender = null;

                SetDangerousControls(enabled: true);
            }
        }

        private void E131toRGB_OSC(object sender, Kadmium_sACN.DataPacket e)
        {
            // Convert each byte (0-255) to float (Float min-Float max)
            var colorFloats = e.DMPLayer.PropertyValues.Take(3)
                .Select(bite => (((float)bite/255)*E131MultiplicationRange) + E131Min).ToArray();

            // Red packet
            if (REnabled)
                Sender.Send(
                    new OscMessage(
                        address: RAddr,
                        colorFloats[0]
                        )
                    );

            // Green packet
            if (GEnabled)
                Sender.Send(
                    new OscMessage(
                        address: GAddr,
                        colorFloats[1]
                        )
                    );

            // Blue packet
            if (BEnabled)
                Sender.Send(
                    new OscMessage(
                        address: BAddr,
                        colorFloats[2]
                        )
                    );
        }

        private void E131toHSL_OSC(object sender, Kadmium_sACN.DataPacket e)
        {
            // Convert each byte (0-255) to int to create an RGB color
            var colorInts = e.DMPLayer.PropertyValues.Take(3)
                .Select(bite => (int)bite);

            Color color = Color.FromArgb(
                red: colorInts.First(),
                green: colorInts.Skip(1).First(),
                blue: colorInts.Last()
                );

            // Get the HSL values and convert them for the min/max selected
            float[] hslFloats = new[] { color.GetHue() / 360, color.GetSaturation(), color.GetBrightness() }
                .Select(val => (val * E131MultiplicationRange) + E131Min).ToArray();

            // Red packet
            if (REnabled)
                Sender.Send(
                    new OscMessage(
                        address: RAddr,
                        hslFloats[0]
                        )
                    );

            // Green packet
            if (GEnabled)
                Sender.Send(
                    new OscMessage(
                        address: GAddr,
                        hslFloats[1]
                        )
                    );

            // Blue packet
            if (BEnabled)
                Sender.Send(
                    new OscMessage(
                        address: BAddr,
                        hslFloats[2]
                        )
                    );
        }

        private async void BtnColorPreview_Click(object sender, EventArgs packet)
        {
            if (E131Reciever is object)
            {
                E131Reciever.OnDataPacketReceived += PanelColorInvoker;
                await Task.Delay(5000);
                // Prevents crash if user disabled e131 while running
                if (E131Reciever is object)
                {
                    E131Reciever.OnDataPacketReceived -= PanelColorInvoker;
                }
            }
        }

        private void PanelColorInvoker(object sender, Kadmium_sACN.DataPacket packet)
        {
            // Make sure the color is set from the UI thread
            PnlColorPreview.Invoke(new Action(() => {
                PnlColorPreview.BackColor =
                    Color.FromArgb(
                        // The first 3 bytes are the actual color
                        red: packet.DMPLayer.PropertyValues.First(),
                        green: packet.DMPLayer.PropertyValues.Skip(1).First(),
                        blue: packet.DMPLayer.PropertyValues.Skip(2).First()
                        );
            }));
        }
    }
}
