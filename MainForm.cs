﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VRC_OSC_Testing
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// Keeps a list of controls that should be disabled while something
        /// is running
        /// </summary>
        private readonly List<Control> DangerousControls;

        public MainForm()
        {
            InitializeComponent();
            DangerousControls = new List<Control>
            {
                // E1.31
                TxtBAddr,
                TxtGAddr,
                TxtRAddr,
                NumFloatMax,
                NumFloatMin,
                NumUniverse,
                RBe131Hsl,
                RBe131Rgb
            };
        }

        // Prevent the user from changing pages while running a demo
        private bool LockTabPages;
        private void TCMain_Selecting(object sender, TabControlCancelEventArgs e)
        {
            e.Cancel = LockTabPages;
        }

        /// <summary>
        /// Lock/unlock controls that don't work or will break something
        /// if changed while running
        /// </summary>
        private void SetDangerousControls(bool enabled)
        {
            foreach (Control control in DangerousControls)
                control.Enabled = enabled;
            LockTabPages = !enabled;
        }

        
    }
}
