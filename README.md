﻿# Open Sound Control Experiments for VRChat

## What is this?

This is a collection of demonstrations of the [Open Sound Control API](https://docs.vrchat.com/v2022.1.1/docs/osc-overview) for the VR Social game VRChat.
### Get it [HERE](https://gitlab.com/davidjhorine/osc-for-vrc/-/releases)

## E1.31 to OSC

This module allows an E1.31 (Also known as Streaming-ACN) stream to be used with VRChat. It subscribes to a single multicast universe and sends the first channel's instructions as a float by either RGB or HSL values. 

### Demo

See a demo in [this tweet thread!](https://twitter.com/davidjhorine/status/1495509507610492928)

### Setting it up

The address you'll most likely want to use is /avatar/parameters/[Parameter Name Here]. See [this documentation](https://docs.vrchat.com/v2022.1.1/docs/osc-avatar-parameters) for more information on finding the proper address to use. If you're controlling parameters usually controlled by a single-axis radial puppet, set the minimum float to 0 and the maximum to 1. If you're controlling parameters usually controlled by a two-axis joystick puppet, set the minimum float to -1 and the maximum to 1.

To use the output to control your avatar's emission color as shown in the example, I recommend using the [Poiyomi Toon Shader](https://github.com/poiyomi/PoiyomiToonShader). See the table below to help in creating the neccecary animation parameters.

| Parameter  | What to animate   | Initial value (float is 0.0) | Final value (float is 1.0) |
|------------|-------------------|------------------------------|----------------------------|
| Hue        | Hue Shift         | 0.0                          | 1.0                        |
| Saturation | Emission Color    | White (RGB 255,255,255)      | Red (RGB 255,0,0)          |
| Lightness  | Emission Strength | 0.0                          | 1.0                        |


For OpenRGB support, go to settings and add an E1.31 device with the following options.

| Setting        | Value                                                        |
|----------------|--------------------------------------------------------------|
| Start Universe | Use the same value as you set in the OSC Experiments program |
| Number of LEDs | 1                                                            |
| IP             | 127.0.0.1                                                    |
| Type           | Single                                                       |
| RGB Order      | RGB                                                          |
| Keepalive Time | 10                                                           |
| Universe Size  | [Leave Blank]                                                |